// Fix the 2 issues in the fetch
async function getRandomString(number, length, format) {
    let response = await fetch("https://www.random.org/strings/?num=" + number + length + format);
    return await response.json();
}

// Call getRandomString() 10 times with increasing by 1 (length++)
getRandomString(1, 16, "plain").then(data => console.log(data));
